package xyz.opcal.cloud.demo.shop.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "storage-service")
public interface StorageServiceClient {

	@GetMapping("/storage/count")
	Integer getCount(@RequestParam String commodityCode);

	@PostMapping("/storage")
	Boolean updateStorage(@RequestParam String commodityCode, @RequestParam Integer count, @RequestParam Boolean error);
}
