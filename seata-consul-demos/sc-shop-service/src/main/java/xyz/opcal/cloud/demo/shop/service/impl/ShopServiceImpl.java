package xyz.opcal.cloud.demo.shop.service.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.shop.common.ErrorType;
import xyz.opcal.cloud.demo.shop.service.ShopService;
import xyz.opcal.cloud.demo.shop.service.client.AccountServiceClient;
import xyz.opcal.cloud.demo.shop.service.client.OrderServiceClient;
import xyz.opcal.cloud.demo.shop.service.client.StorageServiceClient;

@Slf4j
@Service
public class ShopServiceImpl implements ShopService {

	private @Autowired AccountServiceClient accountServiceClient;

	private @Autowired StorageServiceClient storageServiceClient;

	private @Autowired OrderServiceClient orderServiceClient;

	@GlobalTransactional(timeoutMills = 300000, name = "opcal-cloud-alibaba-demo-tx")
	@Override
	public void submit(String userId, String commodityCode, Integer quantity, ErrorType error) {
		log.info("xid {}", RootContext.getXID());
		Integer count = storageServiceClient.getCount(commodityCode);

		BigDecimal money = accountServiceClient.getMoney(userId);

		BigDecimal total = new BigDecimal(10).multiply(new BigDecimal(quantity));

		BigDecimal left = money.subtract(total);

		orderServiceClient.createOrder(userId, commodityCode, quantity, total, ErrorType.isError(ErrorType.ORDER, error));

		accountServiceClient.updateMoney(userId, left, ErrorType.isError(ErrorType.ACCOUNT, error));

		storageServiceClient.updateStorage(commodityCode, count - quantity, ErrorType.isError(ErrorType.STORAGE, error));
	}

}
