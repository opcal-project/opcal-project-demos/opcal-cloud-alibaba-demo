package xyz.opcal.cloud.demo.shop.common;

import java.util.Objects;

public enum ErrorType {

	ACCOUNT, STORAGE, ORDER, NONE;

	public static boolean isError(ErrorType expected, ErrorType errorType) {

		if (Objects.isNull(errorType) || Objects.equals(ErrorType.NONE, errorType)) {
			return false;
		}
		return Objects.equals(expected, errorType);
	}
}
