package xyz.opcal.cloud.demo.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDTO implements Serializable {

    private static final long serialVersionUID = 5182401321431277527L;

    private Long id;
    private String userId;
    private BigDecimal money;
}
