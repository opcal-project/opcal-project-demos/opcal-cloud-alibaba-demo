package xyz.opcal.cloud.demo.order.service;

import java.math.BigDecimal;

import xyz.opcal.cloud.demo.api.dto.OrderDTO;
import xyz.opcal.cloud.demo.order.entity.Order;

public interface OrderService {

	Order findOrder(Long id);

	OrderDTO getOrder(Long id);

	OrderDTO createOrder(String userId, String commodityCode, Integer quantity, BigDecimal total, Boolean error);
}
