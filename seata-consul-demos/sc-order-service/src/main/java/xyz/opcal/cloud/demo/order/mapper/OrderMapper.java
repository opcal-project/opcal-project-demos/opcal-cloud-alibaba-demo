package xyz.opcal.cloud.demo.order.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import xyz.opcal.cloud.demo.api.dto.OrderDTO;
import xyz.opcal.cloud.demo.order.entity.Order;

@Mapper
public interface OrderMapper {

	OrderMapper MAPPER = Mappers.getMapper(OrderMapper.class);

	OrderDTO toDto(Order entity);

	Order toEntity(OrderDTO dto);
}
