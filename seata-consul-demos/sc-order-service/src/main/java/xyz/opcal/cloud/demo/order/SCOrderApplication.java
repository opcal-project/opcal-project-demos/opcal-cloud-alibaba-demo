package xyz.opcal.cloud.demo.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SCOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SCOrderApplication.class, args);
	}

}
