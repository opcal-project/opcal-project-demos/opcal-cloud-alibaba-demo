package xyz.opcal.cloud.demo.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.opcal.cloud.demo.order.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
