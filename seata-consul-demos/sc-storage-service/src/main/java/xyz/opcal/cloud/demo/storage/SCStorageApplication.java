package xyz.opcal.cloud.demo.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SCStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SCStorageApplication.class, args);
	}
}
