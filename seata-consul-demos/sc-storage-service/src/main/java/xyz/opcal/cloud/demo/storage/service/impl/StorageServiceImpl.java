package xyz.opcal.cloud.demo.storage.service.impl;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.api.dto.StorageDTO;
import xyz.opcal.cloud.demo.api.exception.BusinessException;
import xyz.opcal.cloud.demo.storage.entity.Storage;
import xyz.opcal.cloud.demo.storage.mapper.StorageMapper;
import xyz.opcal.cloud.demo.storage.repository.StorageRepository;
import xyz.opcal.cloud.demo.storage.service.StorageService;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class StorageServiceImpl implements StorageService {

	private @Autowired StorageRepository storageRepository;

	@Override
	public Storage findStorage(String commodityCode) {
		return storageRepository.findByCommodityCode(commodityCode);
	}

	@Override
	public StorageDTO getStorage(String commodityCode) {
		return StorageMapper.MAPPER.toDto(findStorage(commodityCode));
	}

	@Override
	public Integer getCount(String commodityCode) {
		Storage storage = findStorage(commodityCode);
		if (Objects.nonNull(storage)) {
			return storage.getCount();
		}
		return 0;
	}

	@Override
	public void updateStorage(String commodityCode, int count, Boolean error) {
		log.info("xid {}", RootContext.getXID());
		Storage storage = findStorage(commodityCode);
		if (Objects.nonNull(storage)) {
			storage.setCount(count);
			storageRepository.save(storage);
		}
		if (Boolean.TRUE.equals(error)) {
			throw new BusinessException("update storage error");
		}
	}

}
