package xyz.opcal.cloud.demo.storage.service;

import xyz.opcal.cloud.demo.api.dto.StorageDTO;
import xyz.opcal.cloud.demo.storage.entity.Storage;

public interface StorageService {

	Storage findStorage(String commodityCode);

	StorageDTO getStorage(String commodityCode);

	Integer getCount(String commodityCode);

	void updateStorage(String commodityCode, int count, Boolean error);
}
