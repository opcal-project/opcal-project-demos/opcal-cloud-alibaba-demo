package xyz.opcal.cloud.demo.account.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.account.service.AccountService;
import xyz.opcal.cloud.demo.api.dto.AccountDTO;

@RestController
public class AccountController {

	private @Autowired AccountService accountService;

	@GetMapping("/account/money")
	public BigDecimal getMoney(String userId) {
		return accountService.getAccountMoney(userId);
	}

	@PostMapping("/account")
	public Boolean updateMoney(String userId, BigDecimal money, Boolean error) {
		accountService.updateUserAccount(userId, money, error);
		return true;
	}

	@GetMapping("/account")
	public AccountDTO getUserAccount(String userId) {
		return accountService.getUserAccount(userId);
	}

}
