package xyz.opcal.cloud.demo.account.service;

import java.math.BigDecimal;

import xyz.opcal.cloud.demo.account.entity.Account;
import xyz.opcal.cloud.demo.api.dto.AccountDTO;

public interface AccountService {

	Account findUserAccount(String userId);

	AccountDTO getUserAccount(String userId);

	BigDecimal getAccountMoney(String userId);

	void updateUserAccount(String userId, BigDecimal money, Boolean error);
}
