# nd-service-a
# /original-api/calculate
echo "nd-service-a /original-api/calculate"
curl -X POST "http://localhost:10380/original-api/calculate?x=9.09&y=3.21"
echo ""

# /cross-api/calculate
echo "nd-service-a /cross-api/calculate"
curl -X POST "http://localhost:10380/cross-api/calculate?x=0.09"
echo ""

# nd-service-b
# /original-api/calculate
echo "nd-service-b /original-api/calculate"
curl -X POST "http://localhost:10381/original-api/calculate?x=0.09&y=3.8&z=12"
echo ""

# /cross-api/calculate
echo "nd-service-b /cross-api/calculate"
curl -X POST "http://localhost:10381/cross-api/calculate?x=0.112"
echo ""

# nd-service-c
# /cross-api/dubbo/calculate
echo "nd-service-c /cross-api/dubbo/calculate"
curl -X POST "http://localhost:10382/cross-api/dubbo/calculate?x=3.8"
echo ""

# /cross-api/b/calculate
echo "nd-service-c /cross-api/b/calculate"
curl -X POST "http://localhost:10382/cross-api/b/calculate?x=99"
echo ""

