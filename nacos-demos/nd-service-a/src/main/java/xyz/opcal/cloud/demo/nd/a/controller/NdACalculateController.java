package xyz.opcal.cloud.demo.nd.a.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/original-api")
public class NdACalculateController {

	@Value("${nd.a.calculator.x-factor}")
	private double xFactor;

	@PostMapping("/calculate")
	public double calculate(@RequestParam Double x, @RequestParam Double y) {

		double result = Math.log1p(x) + Math.pow(x, xFactor) * Math.sin(y);
		log.info("log1p({}) + {}^{} * sin({}) = {}", x, x, xFactor, y, result);
		return result;
	}
}
