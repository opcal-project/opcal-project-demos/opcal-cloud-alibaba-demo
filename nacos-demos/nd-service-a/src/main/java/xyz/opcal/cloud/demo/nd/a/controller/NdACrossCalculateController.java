package xyz.opcal.cloud.demo.nd.a.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.nd.api.service.feign.NdBCalculateApi;

@RestController
@RequestMapping("/cross-api")
public class NdACrossCalculateController {

	@Autowired
	private NdBCalculateApi ndBCalculateApi;

	@PostMapping("/calculate")
	public Double calculate(@RequestParam Double x) {
		return ndBCalculateApi.calculate(x, x, x);
	}
}
