package xyz.opcal.cloud.demo.nd.a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import com.alibaba.csp.sentinel.adapter.dubbo3.config.DubboAdapterGlobalConfig;

import xyz.opcal.cloud.demo.nd.a.fallback.dubbo.NdAConsumerDubboFallback;
import xyz.opcal.cloud.demo.nd.api.service.feign.NdBCalculateApi;
import xyz.opcal.cloud.demo.nd.api.service.feign.configuration.ServiceBFeignConfiguration;

@EnableDiscoveryClient
@Import(ServiceBFeignConfiguration.class)
@EnableFeignClients(clients = { NdBCalculateApi.class })
@SpringBootApplication
public class NdServiceAApplication {

	static {
		DubboAdapterGlobalConfig.setProviderFallback(new NdAConsumerDubboFallback());
	}

	public static void main(String[] args) {
		SpringApplication.run(NdServiceAApplication.class, args);
	}

}
