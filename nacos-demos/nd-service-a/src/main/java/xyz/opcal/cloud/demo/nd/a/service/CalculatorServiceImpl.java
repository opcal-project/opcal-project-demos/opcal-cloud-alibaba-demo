package xyz.opcal.cloud.demo.nd.a.service;

import org.apache.dubbo.config.annotation.DubboService;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.nd.api.service.CalculatorService;

@Slf4j
@DubboService
public class CalculatorServiceImpl implements CalculatorService {

	@Override
	public double calculate(double x, double y, double z) {
		log.info("|{}| - {}^{} * {}", x, y, z, Math.E);
		return Math.abs(x) - Math.pow(y, z) * Math.E;
	}
}
