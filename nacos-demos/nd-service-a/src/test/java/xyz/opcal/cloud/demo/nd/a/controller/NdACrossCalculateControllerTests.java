package xyz.opcal.cloud.demo.nd.a.controller;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@TestInstance(Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class NdACrossCalculateControllerTests {

	private static final AtomicLong atomicLong = new AtomicLong();

	private @Autowired TestRestTemplate testRestTemplate;

	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@BeforeAll
	void init() {
		threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setCorePoolSize(50);
		threadPoolTaskExecutor.initialize();
	}

	@AfterAll
	void stop() {
		threadPoolTaskExecutor.shutdown();
		log.info("NaN counts {}", atomicLong.get());
	}

	@Test
	void parallelTest() {
		Random random = new Random();
		for (int i = 0; i < 1000; i++) {
			threadPoolTaskExecutor.submit(() -> {
				double x = random.nextDouble() + random.nextInt(100);
				Double result = testRestTemplate.postForObject("/cross-api/calculate?x={x}", null, Double.class, x);
				if (Double.isNaN(result)) {
					atomicLong.incrementAndGet();
				}
			});
		}

		while (!threadPoolTaskExecutor.getThreadPoolExecutor().getQueue().isEmpty()) {
		}
	}
}
