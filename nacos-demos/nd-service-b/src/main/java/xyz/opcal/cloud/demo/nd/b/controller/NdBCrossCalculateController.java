package xyz.opcal.cloud.demo.nd.b.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.nd.api.service.feign.NdACalculateApi;

@RestController
@RequestMapping("/cross-api")
public class NdBCrossCalculateController {

	@Autowired
	private NdACalculateApi ndACalculateApi;

	@PostMapping("/calculate")
	public Double calculate(@RequestParam Double x) {
		return ndACalculateApi.calculate(x, Math.log(x));
	}
}
