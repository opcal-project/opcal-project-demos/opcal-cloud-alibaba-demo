package xyz.opcal.cloud.demo.nd.b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import xyz.opcal.cloud.demo.nd.api.service.feign.NdACalculateApi;
import xyz.opcal.cloud.demo.nd.api.service.feign.configuration.ServiceAFeignConfiguration;

@EnableDiscoveryClient
@Import(ServiceAFeignConfiguration.class)
@EnableFeignClients(clients = { NdACalculateApi.class })
@SpringBootApplication
public class NdServiceBApplication {

	public static void main(String[] args) {
		SpringApplication.run(NdServiceBApplication.class, args);
	}

}
