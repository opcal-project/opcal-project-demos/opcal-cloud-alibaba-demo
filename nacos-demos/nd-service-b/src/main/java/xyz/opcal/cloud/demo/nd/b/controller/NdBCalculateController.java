package xyz.opcal.cloud.demo.nd.b.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/original-api")
public class NdBCalculateController {

	@Value("${nd.b.calculator.x-factor}")
	private double xFactor;

	@Value("${nd.b.calculator.y-factor}")
	private double yFactor;

	@Value("${nd.b.calculator.z-factor}")
	private double zFactor;

	@PostMapping("/calculate")
	public double calculate(@RequestParam Double x, @RequestParam Double y, @RequestParam Double z) {
		double result = Math.pow(x, 2) + Math.pow(x, xFactor) * Math.sin(y) * yFactor + Math.pow(zFactor, z);
		log.info("{}^2 + {}^{} * sin({}) * {} + {}^{} = {}", x, x, xFactor, y, yFactor, zFactor, z, result);
		return result;
	}

}
