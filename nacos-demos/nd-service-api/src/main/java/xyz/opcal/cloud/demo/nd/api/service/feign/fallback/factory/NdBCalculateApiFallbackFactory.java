package xyz.opcal.cloud.demo.nd.api.service.feign.fallback.factory;

import org.springframework.cloud.openfeign.FallbackFactory;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.nd.api.service.feign.NdBCalculateApi;

@Slf4j
public class NdBCalculateApiFallbackFactory implements FallbackFactory<NdBCalculateApi> {

	@Override
	public NdBCalculateApi create(Throwable cause) {
		log.error("fallback error exception[{}], cause[{}], message[{}] ", cause.getClass(), cause.getCause(), cause.getMessage());
		return new NdBCalculateApi() {
			@Override
			public double calculate(Double x, Double y, Double z) {
				return Double.NaN;
			}
		};
	}

}
