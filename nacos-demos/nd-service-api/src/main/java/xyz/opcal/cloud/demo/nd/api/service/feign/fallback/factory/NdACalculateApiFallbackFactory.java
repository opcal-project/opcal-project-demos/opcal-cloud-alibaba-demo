package xyz.opcal.cloud.demo.nd.api.service.feign.fallback.factory;

import org.springframework.cloud.openfeign.FallbackFactory;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.nd.api.service.feign.NdACalculateApi;

@Slf4j
public class NdACalculateApiFallbackFactory implements FallbackFactory<NdACalculateApi> {

	@Override
	public NdACalculateApi create(Throwable cause) {
		log.error("fallback error exception[{}], cause[{}], message[{}] ", cause.getClass(), cause.getCause(), cause.getMessage());
		return new NdACalculateApi() {
			@Override
			public double calculate(Double x, Double y) {
				return Double.NaN;
			}
		};
	}
}
