package xyz.opcal.cloud.demo.nd.api.service;

public interface CalculatorService {

    double calculate(double x, double y, double z);

}
