package xyz.opcal.cloud.demo.nd.api.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import xyz.opcal.cloud.demo.nd.api.service.feign.configuration.ServiceBFeignConfiguration;
import xyz.opcal.cloud.demo.nd.api.service.feign.fallback.factory.NdBCalculateApiFallbackFactory;

// @formatter:off
@FeignClient(name = "nd-service-b", path = "/original-api",
		configuration = ServiceBFeignConfiguration.class,
		fallbackFactory = NdBCalculateApiFallbackFactory.class)
// @formatter:on
public interface NdBCalculateApi {

	@PostMapping("/calculate")
	double calculate(@RequestParam Double x, @RequestParam Double y, @RequestParam Double z);

}
