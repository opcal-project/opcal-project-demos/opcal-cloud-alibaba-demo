package xyz.opcal.cloud.demo.nd.api.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import xyz.opcal.cloud.demo.nd.api.service.feign.configuration.ServiceAFeignConfiguration;
import xyz.opcal.cloud.demo.nd.api.service.feign.fallback.factory.NdACalculateApiFallbackFactory;

// @formatter:off
@FeignClient(name = "nd-service-a", path = "/original-api",
		configuration = ServiceAFeignConfiguration.class,
		fallbackFactory = NdACalculateApiFallbackFactory.class)
// @formatter:on
public interface NdACalculateApi {

	@PostMapping("/calculate")
	double calculate(@RequestParam Double x, @RequestParam Double y);
}
