package xyz.opcal.cloud.demo.nd.api.service.feign.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import xyz.opcal.cloud.demo.nd.api.service.feign.fallback.factory.NdACalculateApiFallbackFactory;

@Configuration
public class ServiceAFeignConfiguration {

	@Bean
	public NdACalculateApiFallbackFactory ndACalculateApiFallbackFactory() {
		return new NdACalculateApiFallbackFactory();
	}
}
