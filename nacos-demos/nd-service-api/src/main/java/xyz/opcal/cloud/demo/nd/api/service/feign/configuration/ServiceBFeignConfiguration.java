package xyz.opcal.cloud.demo.nd.api.service.feign.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import xyz.opcal.cloud.demo.nd.api.service.feign.fallback.factory.NdBCalculateApiFallbackFactory;

@Configuration
public class ServiceBFeignConfiguration {

	@Bean
	public NdBCalculateApiFallbackFactory ndBCalculateApiFallbackFactory() {
		return new NdBCalculateApiFallbackFactory();
	}
}
