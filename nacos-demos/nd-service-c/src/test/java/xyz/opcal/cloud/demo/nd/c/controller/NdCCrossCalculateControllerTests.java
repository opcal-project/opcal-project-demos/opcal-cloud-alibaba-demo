package xyz.opcal.cloud.demo.nd.c.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@TestInstance(Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class NdCCrossCalculateControllerTests {

	private @Autowired TestRestTemplate testRestTemplate;

	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	private AtomicLong atomicLong;

	@BeforeEach
	void init() {
		threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setCorePoolSize(50);
		threadPoolTaskExecutor.initialize();
		atomicLong = new AtomicLong();
	}

	@AfterEach
	void stop() {
		threadPoolTaskExecutor.shutdown();
		log.info("NaN counts {}", atomicLong.get());
	}

	@Test
	void dubboTest() {
		Random random = new Random();
		for (int i = 0; i < 1000; i++) {
			threadPoolTaskExecutor.submit(() -> {
				double x = random.nextDouble() + random.nextInt(100);
				ResponseEntity<Double> response = testRestTemplate.postForEntity("/cross-api/dubbo/calculate?x={x}", null, Double.class, x);
				assertEquals(HttpStatus.OK, response.getStatusCode());
				if (Double.isNaN(response.getBody())) {
					atomicLong.incrementAndGet();
				}
			});
		}

		while (!threadPoolTaskExecutor.getThreadPoolExecutor().getQueue().isEmpty()) {
		}
	}

	@Test
	void feignTest() {
		Random random = new Random();
		for (int i = 0; i < 1000; i++) {
			threadPoolTaskExecutor.submit(() -> {
				double x = random.nextDouble() + random.nextInt(100);
				ResponseEntity<Double> response = testRestTemplate.postForEntity("/cross-api/b/calculate?x={x}", null, Double.class, x);
				assertEquals(HttpStatus.OK, response.getStatusCode());
				if (Double.isNaN(response.getBody())) {
					atomicLong.incrementAndGet();
				}
			});
		}

		while (!threadPoolTaskExecutor.getThreadPoolExecutor().getQueue().isEmpty()) {
		}
	}

}
