package xyz.opcal.cloud.demo.nd.c.fallback.dubbo;

import org.apache.dubbo.rpc.AsyncRpcResult;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;

import com.alibaba.csp.sentinel.adapter.dubbo3.fallback.DubboFallback;
import com.alibaba.csp.sentinel.slots.block.BlockException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NdCConsumerDubboFallback implements DubboFallback {

	@Override
	public Result handle(Invoker<?> invoker, Invocation invocation, BlockException ex) {
		log.error("invoker [{}] invocation [{}] ex: ", invoker, invocation, ex);
		return AsyncRpcResult.newDefaultAsyncResult(Double.NaN, invocation);

	}
}
