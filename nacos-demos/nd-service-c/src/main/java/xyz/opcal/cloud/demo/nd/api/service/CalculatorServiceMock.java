package xyz.opcal.cloud.demo.nd.api.service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CalculatorServiceMock implements CalculatorService {

	@Override
	public double calculate(double x, double y, double z) {
		log.warn("mock CalculatorService");
		return Double.NaN;
	}

}
