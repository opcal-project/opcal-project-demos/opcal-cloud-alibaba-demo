package xyz.opcal.cloud.demo.nd.c.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.nd.api.service.CalculatorService;
import xyz.opcal.cloud.demo.nd.api.service.feign.NdBCalculateApi;

@RestController
@RequestMapping("/cross-api")
public class NdCCrossCalculateController {

	@DubboReference(mock = "true")
	private CalculatorService calculatorService;

	@Autowired
	private NdBCalculateApi ndBCalculateApi;

	@PostMapping("/dubbo/calculate")
	public Double dubboCalculate(@RequestParam Double x) {
		return calculatorService.calculate(x, x, x);
	}

	@PostMapping("/b/calculate")
	public Double bCalculate(@RequestParam Double x) {
		return ndBCalculateApi.calculate(x, x, x);
	}
}
