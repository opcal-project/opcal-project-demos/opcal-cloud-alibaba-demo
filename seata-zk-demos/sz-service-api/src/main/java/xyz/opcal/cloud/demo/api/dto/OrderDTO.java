package xyz.opcal.cloud.demo.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDTO implements Serializable {

    private static final long serialVersionUID = -3587020153270519670L;

    private Long id;
    private String userId;
    private String commodityCode;
    private Integer count;
    private BigDecimal money;
}
