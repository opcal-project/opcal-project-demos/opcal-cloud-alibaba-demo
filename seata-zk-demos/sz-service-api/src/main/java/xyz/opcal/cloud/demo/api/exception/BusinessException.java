package xyz.opcal.cloud.demo.api.exception;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 8631811820151262157L;

	public BusinessException(String msg) {
		super(msg);
	}

}
