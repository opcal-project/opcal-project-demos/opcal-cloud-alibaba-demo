package xyz.opcal.cloud.demo.order.service;

import xyz.opcal.cloud.demo.api.service.OrderApiService;
import xyz.opcal.cloud.demo.order.entity.Order;

public interface OrderService extends OrderApiService {

	Order findOrder(Long id);
}
