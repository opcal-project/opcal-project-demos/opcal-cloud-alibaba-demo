package xyz.opcal.cloud.demo.order.service.impl;

import java.math.BigDecimal;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.api.dto.OrderDTO;
import xyz.opcal.cloud.demo.api.exception.BusinessException;
import xyz.opcal.cloud.demo.api.service.OrderApiService;
import xyz.opcal.cloud.demo.order.entity.Order;
import xyz.opcal.cloud.demo.order.mapper.OrderMapper;
import xyz.opcal.cloud.demo.order.repository.OrderRepository;
import xyz.opcal.cloud.demo.order.service.OrderService;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
@DubboService(interfaceClass = OrderApiService.class)
public class OrderServiceImpl implements OrderService {

	private @Autowired OrderRepository orderRepository;

	@Override
	public Order findOrder(Long id) {
		return orderRepository.findById(id).orElse(null);
	}

	@Override
	public OrderDTO getOrder(Long id) {
		return OrderMapper.MAPPER.toDto(findOrder(id));
	}

	@Override
	public OrderDTO createOrder(String userId, String commodityCode, Integer quantity, BigDecimal total, Boolean error) {
		log.info("xid {}", RootContext.getXID());
		Order order = new Order();
		order.setUserId(userId);
		order.setCommodityCode(commodityCode);
		order.setCount(quantity);
		order.setMoney(total);
		orderRepository.save(order);
		if (Boolean.TRUE.equals(error)) {
			throw new BusinessException("crate order error");
		}
		return OrderMapper.MAPPER.toDto(order);
	}
}
