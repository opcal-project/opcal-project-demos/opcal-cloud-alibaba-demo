package xyz.opcal.cloud.demo.order.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.api.dto.OrderDTO;
import xyz.opcal.cloud.demo.order.service.OrderService;

@RestController
public class OrderController {

	private @Autowired OrderService orderService;

	@PostMapping("/order")
	public OrderDTO createOrder(String userId, String commodityCode, Integer quantity, BigDecimal total, Boolean error) {
		return orderService.createOrder(userId, commodityCode, quantity, total, error);
	}
}
