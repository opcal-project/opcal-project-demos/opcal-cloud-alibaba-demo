package xyz.opcal.cloud.demo.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SZOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SZOrderApplication.class, args);
	}

}
