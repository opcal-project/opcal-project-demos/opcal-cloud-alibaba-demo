package xyz.opcal.cloud.demo.shop.service;

import xyz.opcal.cloud.demo.shop.common.ErrorType;

public interface ShopService {

	void submit(String userId, String commodityCode, Integer quantity, ErrorType error);
}
