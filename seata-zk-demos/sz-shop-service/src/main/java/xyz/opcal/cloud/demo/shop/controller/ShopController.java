package xyz.opcal.cloud.demo.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.shop.common.ErrorType;
import xyz.opcal.cloud.demo.shop.service.ShopService;

@RestController
public class ShopController {

	private @Autowired ShopService shopService;

	@PostMapping("/submit")
	public Boolean submit(String userId, String commodityCode, Integer quantity, ErrorType error) {
		shopService.submit(userId, commodityCode, quantity, error);
		return true;
	}

}
