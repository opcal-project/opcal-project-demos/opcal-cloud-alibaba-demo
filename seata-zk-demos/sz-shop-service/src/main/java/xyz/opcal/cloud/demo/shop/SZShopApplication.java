package xyz.opcal.cloud.demo.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class SZShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(SZShopApplication.class, args);
	}
}
