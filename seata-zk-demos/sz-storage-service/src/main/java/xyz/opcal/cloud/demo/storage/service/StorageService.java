package xyz.opcal.cloud.demo.storage.service;

import xyz.opcal.cloud.demo.api.service.StorageApiService;
import xyz.opcal.cloud.demo.storage.entity.Storage;

public interface StorageService extends StorageApiService {

    Storage findStorage(String commodityCode);
}
