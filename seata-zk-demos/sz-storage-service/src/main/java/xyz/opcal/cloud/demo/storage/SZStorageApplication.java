package xyz.opcal.cloud.demo.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SZStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SZStorageApplication.class, args);
	}
}
