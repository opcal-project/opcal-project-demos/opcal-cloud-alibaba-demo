package xyz.opcal.cloud.demo.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.opcal.cloud.demo.account.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

	Account findByUserId(String userId);
}
