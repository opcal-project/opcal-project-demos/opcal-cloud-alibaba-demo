package xyz.opcal.cloud.demo.account.service.impl;

import java.math.BigDecimal;
import java.util.Objects;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.account.entity.Account;
import xyz.opcal.cloud.demo.account.mapper.AccountMapper;
import xyz.opcal.cloud.demo.account.repository.AccountRepository;
import xyz.opcal.cloud.demo.account.service.AccountService;
import xyz.opcal.cloud.demo.api.dto.AccountDTO;
import xyz.opcal.cloud.demo.api.exception.BusinessException;
import xyz.opcal.cloud.demo.api.service.AccountApiService;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
@DubboService(interfaceClass = AccountApiService.class)
public class AccountServiceImpl implements AccountService {

	private @Autowired AccountRepository accountRepository;

	@Override
	public Account findUserAccount(String userId) {
		return accountRepository.findByUserId(userId);
	}

	@Override
	public AccountDTO getUserAccount(String userId) {
		return AccountMapper.MAPPER.toDto(findUserAccount(userId));
	}

	@Override
	public BigDecimal getAccountMoney(String userId) {
		Account account = findUserAccount(userId);

		if (Objects.nonNull(account)) {
			return account.getMoney();
		}
		return BigDecimal.ZERO;
	}

	@Override
	public void updateUserAccount(String userId, BigDecimal money, Boolean error) {
		log.info("xid {}", RootContext.getXID());
		Account account = findUserAccount(userId);
		if (Objects.nonNull(account)) {
			account.setMoney(money);
			accountRepository.save(account);
		}
		if (Boolean.TRUE.equals(error)) {
			throw new BusinessException("account update error");
		}
	}
}
