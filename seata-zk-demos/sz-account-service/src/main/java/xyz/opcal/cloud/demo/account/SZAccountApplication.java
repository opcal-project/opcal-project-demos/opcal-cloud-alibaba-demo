package xyz.opcal.cloud.demo.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SZAccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(SZAccountApplication.class, args);
	}

}
