package xyz.opcal.cloud.demo.account.service;

import xyz.opcal.cloud.demo.account.entity.Account;
import xyz.opcal.cloud.demo.api.service.AccountApiService;

public interface AccountService extends AccountApiService {

	Account findUserAccount(String userId);

}
