package xyz.opcal.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.images.builder.ImageFromDockerfile;
import org.testcontainers.lifecycle.Startables;

import lombok.SneakyThrows;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class IntegrationNacosServiceTests {

	static String tag = String.valueOf(System.currentTimeMillis());
	static String rootPath = System.getenv("PROJECT_DIR");

	static GenericContainer<?> serviceA = new GenericContainer<>(new ImageFromDockerfile("nd-service-a:" + tag, true) //
			.withFileFromFile("/tmp/artifact/nd-service-a.jar", new File("/tmp/artifact/nd-service-a.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-nacos-services/docker/nd-service-a/Dockerfile").toFile())//
	).withExposedPorts(10380);

	static GenericContainer<?> serviceB = new GenericContainer<>(new ImageFromDockerfile("nd-service-b:" + tag, true) //
			.withFileFromFile("/tmp/artifact/nd-service-b.jar", new File("/tmp/artifact/nd-service-b.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-nacos-services/docker/nd-service-b/Dockerfile").toFile())//
	).withExposedPorts(10381);

	static GenericContainer<?> serviceC = new GenericContainer<>(new ImageFromDockerfile("nd-service-c:" + tag, true) //
			.withFileFromFile("/tmp/artifact/nd-service-c.jar", new File("/tmp/artifact/nd-service-c.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-nacos-services/docker/nd-service-c/Dockerfile").toFile())//
	).withExposedPorts(10382);

	static String service_a_url;
	static String service_b_url;
	static String service_c_url;

	@SneakyThrows
	@DynamicPropertySource
	static void redisProperties(DynamicPropertyRegistry registry) {
		Startables.deepStart(serviceA, serviceB, serviceC).join();

		service_a_url = "http://" + serviceA.getHost() + ":" + serviceA.getFirstMappedPort();
		service_b_url = "http://" + serviceB.getHost() + ":" + serviceB.getFirstMappedPort();
		service_c_url = "http://" + serviceC.getHost() + ":" + serviceC.getFirstMappedPort();

		Thread.sleep(10000); // wait for dubbo register
	}

	RestTemplate restTemplate;

	@BeforeAll
	void init() {
		restTemplate = new RestTemplate();
	}

	@Test
	@Order(0)
	void testServiceA() {

		Map<String, String> param1 = new HashMap<>();
		param1.put("x", "9.09");
		param1.put("y", "3.21");

		ResponseEntity<String> response1 = restTemplate.postForEntity(service_a_url + "/original-api/calculate?x={x}&y={y}", null, String.class, param1);

		assertEquals(HttpStatus.OK, response1.getStatusCode());

		System.out.println("service-a response1: " + response1.getBody());

		Map<String, String> param2 = new HashMap<>();
		param2.put("x", "0.09");

		ResponseEntity<String> response2 = restTemplate.postForEntity(service_a_url + "/cross-api/calculate?x={x}", null, String.class, param2);

		assertEquals(HttpStatus.OK, response2.getStatusCode());

		System.out.println("service-a response2: " + response2.getBody());

	}

	@Test
	@Order(1)
	void testServiceB() {
		Map<String, String> param1 = new HashMap<>();
		param1.put("x", "0.09");
		param1.put("y", "3.8");
		param1.put("z", "12");

		ResponseEntity<String> response1 = restTemplate.postForEntity(service_b_url + "/original-api/calculate?x={x}&y={y}&z={z}", null, String.class, param1);

		assertEquals(HttpStatus.OK, response1.getStatusCode());

		System.out.println("service-b response1: " + response1.getBody());

		Map<String, String> param2 = new HashMap<>();
		param2.put("x", "0.112");

		ResponseEntity<String> response2 = restTemplate.postForEntity(service_b_url + "/cross-api/calculate?x={x}", null, String.class, param2);

		assertEquals(HttpStatus.OK, response2.getStatusCode());

		System.out.println("service-b response2: " + response2.getBody());
	}

	@Test
	@Order(2)
	void testServiceC() {
		Map<String, String> param1 = new HashMap<>();
		param1.put("x", "3.8");

		ResponseEntity<String> response1 = restTemplate.postForEntity(service_c_url + "/cross-api/dubbo/calculate?x={x}", null, String.class, param1);

		assertEquals(HttpStatus.OK, response1.getStatusCode());

		System.out.println("service-c response1: " + response1.getBody());

		Map<String, String> param2 = new HashMap<>();
		param2.put("x", "99");

		ResponseEntity<String> response2 = restTemplate.postForEntity(service_c_url + "/cross-api/b/calculate?x={x}", null, String.class, param2);

		assertEquals(HttpStatus.OK, response2.getStatusCode());

		System.out.println("service-c response2: " + response2.getBody());
	}

}
