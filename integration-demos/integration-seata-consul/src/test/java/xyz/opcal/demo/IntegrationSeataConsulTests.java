package xyz.opcal.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.images.builder.ImageFromDockerfile;
import org.testcontainers.lifecycle.Startables;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class IntegrationSeataConsulTests {

	static final String FEIGN_SUBMIT_URL = "/submit?userId={userId}&commodityCode={commodityCode}&quantity={quantity}&error={error}";
	static final String SHUTDOWN_URL = "/actuator/shutdown";

	static final String USER_ID = "U100001";
	static final String COMMODITY_CODE = "C00321";

	static String tag = String.valueOf(System.currentTimeMillis());
	static String rootPath = System.getenv("PROJECT_DIR");

	static MySQLContainer<?> mysql = new MySQLContainer<>(DockerImageName.parse("mysql:8.0"))
			.withCopyFileToContainer(MountableFile.forHostPath(Paths.get(rootPath, "integration-demos/integration-seata-consul/sql/")),
					"/docker-entrypoint-initdb.d/")
			.withCommand("--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci").withExposedPorts(3306)
			.withUrlParam("characterEncoding", "utf-8").withUrlParam("autoReconnect", "true").withUrlParam("useSSL", "false")
			.withUrlParam("allowPublicKeyRetrieval", "true");

	static Map<String, String> tcProfile = new HashMap<>();

	static GenericContainer<?> accountService = new GenericContainer<>(new ImageFromDockerfile("account-service:" + tag, true) //
			.withFileFromFile("/tmp/artifact/sc-account-service.jar", new File("/tmp/artifact/sc-account-service.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-seata-consul/docker/account-service/Dockerfile").toFile())//
	).withExposedPorts(10683);

	static GenericContainer<?> storageService = new GenericContainer<>(new ImageFromDockerfile("storage-service:" + tag, true) //
			.withFileFromFile("/tmp/artifact/sc-storage-service.jar", new File("/tmp/artifact/sc-storage-service.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-seata-consul/docker/storage-service/Dockerfile").toFile())//
	).withExposedPorts(10684);

	static GenericContainer<?> orderService = new GenericContainer<>(new ImageFromDockerfile("order-service:" + tag, true) //
			.withFileFromFile("/tmp/artifact/sc-order-service.jar", new File("/tmp/artifact/sc-order-service.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-seata-consul/docker/order-service/Dockerfile").toFile())//
	).withExposedPorts(10685);

	static GenericContainer<?> shopService = new GenericContainer<>(new ImageFromDockerfile("shop-service:" + tag, true) //
			.withFileFromFile("/tmp/artifact/sc-shop-service.jar", new File("/tmp/artifact/sc-shop-service.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-seata-consul/docker/shop-service/Dockerfile").toFile())//
	).withExposedPorts(10686);

	static String accountServiceUrl;
	static String storageServiceUrl;
	static String orderServiceUrl;
	static String shopServiceUrl;

	@DynamicPropertySource
	static void redisProperties(DynamicPropertyRegistry registry) {
		Startables.deepStart(mysql).join();
		tcProfile.put("SPRING_PROFILES_ACTIVE", "integration");
		tcProfile.put("INTEGRATION_DB_URL", mysql.getJdbcUrl());
		tcProfile.put("INTEGRATION_DB_USER", mysql.getUsername());
		tcProfile.put("INTEGRATION_DB_PASSWORD", mysql.getPassword());

		registry.add("spring.datasource.url", mysql::getJdbcUrl);
		registry.add("spring.datasource.username", mysql::getUsername);
		registry.add("spring.datasource.password", mysql::getPassword);

		accountService.withEnv(tcProfile);
		storageService.withEnv(tcProfile);
		orderService.withEnv(tcProfile);
		shopService.withEnv(tcProfile);

		Startables.deepStart(accountService, storageService, orderService, shopService).join();
		accountServiceUrl = "http://" + accountService.getHost() + ":" + accountService.getFirstMappedPort();
		storageServiceUrl = "http://" + storageService.getHost() + ":" + storageService.getFirstMappedPort();
		orderServiceUrl = "http://" + orderService.getHost() + ":" + orderService.getFirstMappedPort();
		shopServiceUrl = "http://" + shopService.getHost() + ":" + shopService.getFirstMappedPort();

		try {
			Thread.sleep(10000); // waiting services are available after registered
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	RestTemplate restTemplate;
	@Autowired
	JdbcTemplate jdbcTemplate;

	@BeforeAll
	void init() {
		restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new ResponseErrorHandler() {

			@Override
			public boolean hasError(ClientHttpResponse response) throws IOException {
				return false;
			}

			@Override
			public void handleError(ClientHttpResponse response) throws IOException {
				// do nothing
			}
		});

	}

	@AfterAll
	void stop() {
		stopContainers();
		List<Map<String, Object>> accounts = jdbcTemplate.queryForList("SELECT * FROM account_tbl");
		System.out.println("account info: \n" + accounts);

		List<Map<String, Object>> storages = jdbcTemplate.queryForList("SELECT * FROM storage_tbl");
		System.out.println("storage info: \n" + storages);
	}

	/**
	 * because testcontainer using killcommand
	 */
	void stopContainers() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(headers);
		restTemplate.postForObject(accountServiceUrl + SHUTDOWN_URL, request, String.class);
		restTemplate.postForObject(storageServiceUrl + SHUTDOWN_URL, request, String.class);
		restTemplate.postForObject(orderServiceUrl + SHUTDOWN_URL, request, String.class);
		restTemplate.postForObject(shopServiceUrl + SHUTDOWN_URL, request, String.class);
	}

	@Test
	@Order(0)
	void feignTest() {
		int quantity = new Random().nextInt(50) + 1;
		log.info("feignTest quantity : {} ", quantity);
		ResponseEntity<String> result = restTemplate.postForEntity(shopServiceUrl + FEIGN_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity,
				"NONE");
		assertEquals(HttpStatus.OK, result.getStatusCode());
		log.info("feign ok : {} ", result.getBody());
	}

	@Test
	@Order(1)
	void feignRollbackTest() {
		int quantity = new Random().nextInt(50) + 1;
		log.info("feignRollbackTest quantity : {} ", quantity);
		ResponseEntity<String> result = restTemplate.postForEntity(shopServiceUrl + FEIGN_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity,
				"ORDER");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("feign ORDER error : {} ", result.getBody());

		result = restTemplate.postForEntity(shopServiceUrl + FEIGN_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, "ACCOUNT");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("feign ACCOUNT error : {} ", result.getBody());

		result = restTemplate.postForEntity(shopServiceUrl + FEIGN_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, "STORAGE");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("feign STORAGE error : {} ", result.getBody());

	}

}
