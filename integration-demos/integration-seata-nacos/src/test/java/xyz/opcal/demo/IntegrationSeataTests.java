package xyz.opcal.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.images.builder.ImageFromDockerfile;
import org.testcontainers.lifecycle.Startables;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class IntegrationSeataTests {

	static final String FEIGN_SUBMIT_URL = "/submit?userId={userId}&commodityCode={commodityCode}&quantity={quantity}&error={error}";
	static final String DUBBO_SUBMIT_URL = "/dubbo/submit?userId={userId}&commodityCode={commodityCode}&quantity={quantity}&error={error}";
	static final String HYBRID_SUBMIT_URL = "/hybrid/submit?userId={userId}&commodityCode={commodityCode}&quantity={quantity}&error={error}";

	static final String USER_ID = "U100001";
	static final String COMMODITY_CODE = "C00321";

	static String tag = String.valueOf(System.currentTimeMillis());
	static String rootPath = System.getenv("PROJECT_DIR");

	static MySQLContainer<?> mysql = new MySQLContainer<>(DockerImageName.parse("mysql:8.0"))
			.withCopyFileToContainer(MountableFile.forHostPath(Paths.get(rootPath, "integration-demos/integration-seata-nacos/sql/")), "/docker-entrypoint-initdb.d/")
			.withCommand("--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci").withExposedPorts(3306);

	static Map<String, String> tcProfile = new HashMap<>();

	static GenericContainer<?> accountService = new GenericContainer<>(new ImageFromDockerfile("account-service:" + tag, true) //
			.withFileFromFile("/tmp/artifact/account-service.jar", new File("/tmp/artifact/account-service.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-seata-nacos/docker/account-service/Dockerfile").toFile())//
	).withExposedPorts(10383);

	static GenericContainer<?> storageService = new GenericContainer<>(new ImageFromDockerfile("storage-service:" + tag, true) //
			.withFileFromFile("/tmp/artifact/storage-service.jar", new File("/tmp/artifact/storage-service.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-seata-nacos/docker/storage-service/Dockerfile").toFile())//
	).withExposedPorts(10384);

	static GenericContainer<?> orderService = new GenericContainer<>(new ImageFromDockerfile("order-service:" + tag, true) //
			.withFileFromFile("/tmp/artifact/order-service.jar", new File("/tmp/artifact/order-service.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-seata-nacos/docker/order-service/Dockerfile").toFile())//
	).withExposedPorts(10385);

	static GenericContainer<?> shopService = new GenericContainer<>(new ImageFromDockerfile("shop-service:" + tag, true) //
			.withFileFromFile("/tmp/artifact/shop-service.jar", new File("/tmp/artifact/shop-service.jar")) //
			.withFileFromFile("Dockerfile", Paths.get(rootPath, "integration-demos/integration-seata-nacos/docker/shop-service/Dockerfile").toFile())//
	).withExposedPorts(10386);

	static String shopServiceUrl;

	@DynamicPropertySource
	static void redisProperties(DynamicPropertyRegistry registry) {
		Startables.deepStart(mysql).join();
		tcProfile.put("SPRING_PROFILES_ACTIVE", "tc");
		tcProfile.put("TC_MYSQL_HOST", mysql.getHost());
		tcProfile.put("TC_MYSQL_PORT", mysql.getFirstMappedPort().toString());

		System.setProperty("INTEGRATION_MYSQL_HOST", mysql.getHost());
		System.setProperty("INTEGRATION_MYSQL_PORT", mysql.getFirstMappedPort().toString());
		
		accountService.withEnv(tcProfile);
		storageService.withEnv(tcProfile);
		orderService.withEnv(tcProfile);
		shopService.withEnv(tcProfile);
		
		Startables.deepStart(accountService, storageService, orderService, shopService).join();
		shopServiceUrl = "http://" + shopService.getHost() + ":" + shopService.getFirstMappedPort();
	}

	RestTemplate restTemplate;
	@Autowired
	JdbcTemplate jdbcTemplate;

	@BeforeAll
	void init() {
		restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new ResponseErrorHandler() {

			@Override
			public boolean hasError(ClientHttpResponse response) throws IOException {
				return false;
			}

			@Override
			public void handleError(ClientHttpResponse response) throws IOException {
				// do nothing
			}
		});
	}

	@AfterAll
	void stop() {
		List<Map<String, Object>> accounts = jdbcTemplate.queryForList("SELECT * FROM account_tbl");
		System.out.println("account info: \n" + accounts);

		List<Map<String, Object>> storages = jdbcTemplate.queryForList("SELECT * FROM storage_tbl");
		System.out.println("storage info: \n" + storages);
	}

	@Test
	@Order(0)
	void feignTest() {
		int quantity = new Random().nextInt(50) + 1;
		log.info("feignTest quantity : {} ", quantity);
		ResponseEntity<String> result = restTemplate.postForEntity(shopServiceUrl + FEIGN_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity,
				"NONE");
		assertEquals(HttpStatus.OK, result.getStatusCode());
		log.info("feign ok : {} ", result.getBody());
	}

	@Test
	@Order(1)
	void feignRollbackTest() {
		int quantity = new Random().nextInt(50) + 1;
		log.info("feignRollbackTest quantity : {} ", quantity);
		ResponseEntity<String> result = restTemplate.postForEntity(shopServiceUrl + FEIGN_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity,
				"ORDER");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("feign ORDER error : {} ", result.getBody());

		result = restTemplate.postForEntity(shopServiceUrl + FEIGN_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, "ACCOUNT");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("feign ACCOUNT error : {} ", result.getBody());

		result = restTemplate.postForEntity(shopServiceUrl + FEIGN_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, "STORAGE");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("feign STORAGE error : {} ", result.getBody());

	}

	@Test
	@Order(2)
	void dubboTest() {
		int quantity = new Random().nextInt(50) + 1;
		log.info("dubboTest quantity : {} ", quantity);
		ResponseEntity<String> result = restTemplate.postForEntity(shopServiceUrl + DUBBO_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity,
				"NONE");
		assertEquals(HttpStatus.OK, result.getStatusCode());
		log.info("dubbo ok : {} ", result.getBody());
	}

	@Test
	@Order(3)
	void dubboRollbackTest() {
		int quantity = new Random().nextInt(50) + 1;
		log.info("dubboRollbackTest quantity : {} ", quantity);
		ResponseEntity<String> result = restTemplate.postForEntity(shopServiceUrl + DUBBO_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity,
				"ORDER");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("dubbo ORDER error : {} ", result.getBody());

		result = restTemplate.postForEntity(shopServiceUrl + DUBBO_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, "ACCOUNT");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("dubbo ACCOUNT error : {} ", result.getBody());

		result = restTemplate.postForEntity(shopServiceUrl + DUBBO_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, "STORAGE");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("dubbo STORAGE error : {} ", result.getBody());
	}

	@Test
	@Order(4)
	void hybridTest() {
		int quantity = new Random().nextInt(50) + 1;
		log.info("hybridTest quantity : {} ", quantity);
		ResponseEntity<String> result = restTemplate.postForEntity(shopServiceUrl + HYBRID_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity,
				"NONE");
		assertEquals(HttpStatus.OK, result.getStatusCode());
		log.info("hybrid ok : {} ", result.getBody());
	}

	@Test
	@Order(5)
	void hybridRollbackTest() {
		int quantity = new Random().nextInt(50) + 1;
		log.info("hybridRollbackTest quantity : {} ", quantity);
		ResponseEntity<String> result = restTemplate.postForEntity(shopServiceUrl + HYBRID_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity,
				"ORDER");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("hybrid ORDER error : {} ", result.getBody());

		result = restTemplate.postForEntity(shopServiceUrl + HYBRID_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, "ACCOUNT");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("hybrid ACCOUNT error : {} ", result.getBody());

		result = restTemplate.postForEntity(shopServiceUrl + HYBRID_SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, "STORAGE");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		log.info("hybrid STORAGE error : {} ", result.getBody());
	}

}
