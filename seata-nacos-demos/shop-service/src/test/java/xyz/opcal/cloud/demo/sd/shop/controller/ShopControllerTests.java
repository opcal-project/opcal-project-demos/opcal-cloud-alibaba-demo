package xyz.opcal.cloud.demo.sd.shop.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Random;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import xyz.opcal.cloud.demo.sd.shop.common.ErrorType;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ShopControllerTests {

	static final String SUBMIT_URL = "/submit?userId={userId}&commodityCode={commodityCode}&quantity={quantity}&error={error}";
	static final String USER_ID = "U100001";
	static final String COMMODITY_CODE = "C00321";

	private @Autowired TestRestTemplate testRestTemplate;

	@Test
	@Order(1)
	void test() {
		int quantity = new Random().nextInt(50) + 1;
		ResponseEntity<String> result = testRestTemplate.postForEntity(SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, ErrorType.NONE);
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	@Order(2)
	void rollback() {
		int quantity = new Random().nextInt(50) + 1;
		ResponseEntity<String> result = testRestTemplate.postForEntity(SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, ErrorType.ORDER);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		result = testRestTemplate.postForEntity(SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, ErrorType.ACCOUNT);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		result = testRestTemplate.postForEntity(SUBMIT_URL, null, String.class, USER_ID, COMMODITY_CODE, quantity, ErrorType.STORAGE);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
	}

}
