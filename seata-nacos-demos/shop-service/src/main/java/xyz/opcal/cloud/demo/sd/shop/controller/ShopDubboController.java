package xyz.opcal.cloud.demo.sd.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.sd.shop.common.ErrorType;
import xyz.opcal.cloud.demo.sd.shop.service.ShopDubboService;

@RestController
public class ShopDubboController {

	private @Autowired ShopDubboService shopDubboService;

	@PostMapping("/dubbo/submit")
	public Boolean submit(String userId, String commodityCode, Integer quantity, ErrorType error) {
		shopDubboService.submit(userId, commodityCode, quantity, error);
		return true;
	}

}
