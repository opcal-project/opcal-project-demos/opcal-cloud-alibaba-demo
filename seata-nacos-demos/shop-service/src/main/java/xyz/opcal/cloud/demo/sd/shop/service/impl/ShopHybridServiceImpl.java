package xyz.opcal.cloud.demo.sd.shop.service.impl;

import java.math.BigDecimal;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.api.service.OrderApiService;
import xyz.opcal.cloud.demo.sd.shop.common.ErrorType;
import xyz.opcal.cloud.demo.sd.shop.service.ShopHybridService;
import xyz.opcal.cloud.demo.sd.shop.service.client.AccountServiceClient;
import xyz.opcal.cloud.demo.sd.shop.service.client.StorageServiceClient;

@Slf4j
@Service
public class ShopHybridServiceImpl implements ShopHybridService {

	private @Autowired AccountServiceClient accountServiceClient;

	private @Autowired StorageServiceClient storageServiceClient;

	@DubboReference
	private OrderApiService orderApiService;

	@GlobalTransactional(timeoutMills = 300000, name = "opcal-cloud-alibaba-demo-tx")
	@Override
	public void submit(String userId, String commodityCode, Integer quantity, ErrorType error) {

		log.info("xid {}", RootContext.getXID());

		BigDecimal money = accountServiceClient.getMoney(userId);

		Integer count = storageServiceClient.getCount(commodityCode);

		BigDecimal total = new BigDecimal(10).multiply(new BigDecimal(quantity));

		orderApiService.createOrder(userId, commodityCode, quantity, total, ErrorType.isError(ErrorType.ORDER, error));

		BigDecimal left = money.subtract(total);

		accountServiceClient.updateMoney(userId, left, ErrorType.isError(ErrorType.ACCOUNT, error));

		storageServiceClient.updateStorage(commodityCode, count - quantity, ErrorType.isError(ErrorType.STORAGE, error));

	}
}
