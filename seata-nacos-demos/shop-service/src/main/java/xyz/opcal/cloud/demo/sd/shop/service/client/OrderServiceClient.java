package xyz.opcal.cloud.demo.sd.shop.service.client;

import java.math.BigDecimal;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import xyz.opcal.cloud.demo.api.dto.OrderDTO;

@FeignClient(name = "order-service")
public interface OrderServiceClient {

	@PostMapping("/order")
	OrderDTO createOrder(@RequestParam String userId,
						 @RequestParam String commodityCode,
						 @RequestParam Integer quantity,
						 @RequestParam BigDecimal total,
						 @RequestParam Boolean error);

}
