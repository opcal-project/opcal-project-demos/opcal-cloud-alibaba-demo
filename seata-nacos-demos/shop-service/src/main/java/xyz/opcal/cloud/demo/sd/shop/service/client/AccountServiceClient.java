package xyz.opcal.cloud.demo.sd.shop.service.client;

import java.math.BigDecimal;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "account-service")
public interface AccountServiceClient {

	@GetMapping("/account/money")
	BigDecimal getMoney(@RequestParam String userId);

	@PostMapping("/account")
	Boolean updateMoney(@RequestParam String userId, @RequestParam BigDecimal money, @RequestParam Boolean error);

}
