package xyz.opcal.cloud.demo.sd.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.sd.shop.common.ErrorType;
import xyz.opcal.cloud.demo.sd.shop.service.ShopHybridService;

@RestController
public class ShopHybridController {

	private @Autowired ShopHybridService shopHybridService;

	@PostMapping("/hybrid/submit")
	public Boolean submit(String userId, String commodityCode, Integer quantity, ErrorType error) {
		shopHybridService.submit(userId, commodityCode, quantity, error);
		return true;
	}

}
