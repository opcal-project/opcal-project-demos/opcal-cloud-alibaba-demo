package xyz.opcal.cloud.demo.sd.shop.service;

import xyz.opcal.cloud.demo.sd.shop.common.ErrorType;

public interface ShopService {

	void submit(String userId, String commodityCode, Integer quantity, ErrorType error);
}
