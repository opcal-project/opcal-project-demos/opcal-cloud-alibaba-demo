package xyz.opcal.cloud.demo.sd.shop.service.impl;

import java.math.BigDecimal;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import xyz.opcal.cloud.demo.api.service.AccountApiService;
import xyz.opcal.cloud.demo.api.service.OrderApiService;
import xyz.opcal.cloud.demo.api.service.StorageApiService;
import xyz.opcal.cloud.demo.sd.shop.common.ErrorType;
import xyz.opcal.cloud.demo.sd.shop.service.ShopDubboService;

@Slf4j
@Service
public class ShopDubboServiceImpl implements ShopDubboService {

	@DubboReference
	private AccountApiService accountApiService;

	@DubboReference
	private StorageApiService storageApiService;

	@DubboReference
	private OrderApiService orderApiService;

	@GlobalTransactional(timeoutMills = 300000, name = "opcal-cloud-alibaba-demo-tx")
	@Override
	public void submit(String userId, String commodityCode, Integer quantity, ErrorType error) {
		log.info("xid {}", RootContext.getXID());

		Integer count = storageApiService.getCount(commodityCode);

		BigDecimal money = accountApiService.getAccountMoney(userId);

		BigDecimal total = new BigDecimal(10).multiply(new BigDecimal(quantity));

		BigDecimal left = money.subtract(total);

		orderApiService.createOrder(userId, commodityCode, quantity, total, ErrorType.isError(ErrorType.ORDER, error));

		accountApiService.updateUserAccount(userId, left, ErrorType.isError(ErrorType.ACCOUNT, error));

		storageApiService.updateStorage(commodityCode, count - quantity, ErrorType.isError(ErrorType.STORAGE, error));
	}
}
