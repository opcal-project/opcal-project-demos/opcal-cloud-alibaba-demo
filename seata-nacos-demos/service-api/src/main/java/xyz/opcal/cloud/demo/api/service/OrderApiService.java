package xyz.opcal.cloud.demo.api.service;

import java.math.BigDecimal;

import xyz.opcal.cloud.demo.api.dto.OrderDTO;

public interface OrderApiService {

	OrderDTO getOrder(Long id);

	OrderDTO createOrder(String userId, String commodityCode, Integer quantity, BigDecimal total, Boolean error);
}
