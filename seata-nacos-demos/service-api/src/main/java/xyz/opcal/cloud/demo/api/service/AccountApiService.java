package xyz.opcal.cloud.demo.api.service;

import java.math.BigDecimal;
import xyz.opcal.cloud.demo.api.dto.AccountDTO;

public interface AccountApiService {

    AccountDTO getUserAccount(String userId);

    BigDecimal getAccountMoney(String userId);

    void updateUserAccount(String userId, BigDecimal money, Boolean error);

}
