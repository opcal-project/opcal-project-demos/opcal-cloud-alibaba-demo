package xyz.opcal.cloud.demo.api.service;

import xyz.opcal.cloud.demo.api.dto.StorageDTO;

public interface StorageApiService {

	StorageDTO getStorage(String commodityCode);

	Integer getCount(String commodityCode);

	void updateStorage(String commodityCode, int count, Boolean error);
}
