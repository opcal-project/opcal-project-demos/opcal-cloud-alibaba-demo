package xyz.opcal.cloud.demo.api.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StorageDTO implements Serializable {

	private static final long serialVersionUID = -5904575642420257998L;

	private Long id;
	private String commodityCode;
	private Integer count;
}
