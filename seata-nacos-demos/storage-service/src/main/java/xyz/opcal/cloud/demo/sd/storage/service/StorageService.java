package xyz.opcal.cloud.demo.sd.storage.service;

import xyz.opcal.cloud.demo.api.service.StorageApiService;
import xyz.opcal.cloud.demo.sd.storage.entity.Storage;

public interface StorageService extends StorageApiService {

    Storage findStorage(String commodityCode);
}
