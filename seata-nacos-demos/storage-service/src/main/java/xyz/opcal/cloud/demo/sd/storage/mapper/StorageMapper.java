package xyz.opcal.cloud.demo.sd.storage.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import xyz.opcal.cloud.demo.api.dto.StorageDTO;
import xyz.opcal.cloud.demo.sd.storage.entity.Storage;

@Mapper
public interface StorageMapper {

	StorageMapper MAPPER = Mappers.getMapper(StorageMapper.class);

	StorageDTO toDto(Storage entity);

	Storage toEntity(StorageDTO dto);
}
