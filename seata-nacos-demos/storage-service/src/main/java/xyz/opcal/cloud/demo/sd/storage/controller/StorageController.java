package xyz.opcal.cloud.demo.sd.storage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.cloud.demo.api.dto.StorageDTO;
import xyz.opcal.cloud.demo.sd.storage.service.StorageService;

@RestController
public class StorageController {

	private @Autowired StorageService storageService;

	@GetMapping("/storage/count")
	public Integer getCount(String commodityCode) {
		return storageService.getCount(commodityCode);
	}

	@PostMapping("/storage")
	public Boolean updateStorage(String commodityCode, Integer count, Boolean error) {
		storageService.updateStorage(commodityCode, count, error);
		return true;
	}

	@GetMapping("/storage")
	public StorageDTO getStorage(String commodityCode) {
		return storageService.getStorage(commodityCode);
	}

}
