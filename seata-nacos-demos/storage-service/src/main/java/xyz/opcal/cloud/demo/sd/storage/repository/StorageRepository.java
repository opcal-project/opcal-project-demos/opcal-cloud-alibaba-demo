package xyz.opcal.cloud.demo.sd.storage.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.opcal.cloud.demo.sd.storage.entity.Storage;

public interface StorageRepository extends JpaRepository<Storage, Long> {

	Storage findByCommodityCode(String commodityCode);

}
