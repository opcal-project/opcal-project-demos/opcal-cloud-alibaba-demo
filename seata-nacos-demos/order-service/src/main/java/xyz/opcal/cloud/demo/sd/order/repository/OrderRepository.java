package xyz.opcal.cloud.demo.sd.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.opcal.cloud.demo.sd.order.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
