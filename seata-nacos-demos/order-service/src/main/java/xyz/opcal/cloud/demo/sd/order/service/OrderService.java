package xyz.opcal.cloud.demo.sd.order.service;

import xyz.opcal.cloud.demo.api.service.OrderApiService;
import xyz.opcal.cloud.demo.sd.order.entity.Order;

public interface OrderService extends OrderApiService {

	Order findOrder(Long id);
}
