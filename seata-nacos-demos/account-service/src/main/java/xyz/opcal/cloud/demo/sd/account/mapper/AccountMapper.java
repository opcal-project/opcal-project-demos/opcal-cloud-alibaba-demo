package xyz.opcal.cloud.demo.sd.account.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import xyz.opcal.cloud.demo.api.dto.AccountDTO;
import xyz.opcal.cloud.demo.sd.account.entity.Account;

@Mapper
public interface AccountMapper {

	AccountMapper MAPPER = Mappers.getMapper(AccountMapper.class);

	AccountDTO toDto(Account entity);

	Account toEntity(AccountDTO dto);
}
