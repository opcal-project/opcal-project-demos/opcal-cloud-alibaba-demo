package xyz.opcal.cloud.demo.sd.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.opcal.cloud.demo.sd.account.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

	Account findByUserId(String userId);
}
