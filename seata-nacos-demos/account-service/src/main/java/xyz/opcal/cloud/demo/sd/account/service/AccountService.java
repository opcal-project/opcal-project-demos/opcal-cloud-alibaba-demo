package xyz.opcal.cloud.demo.sd.account.service;

import xyz.opcal.cloud.demo.api.service.AccountApiService;
import xyz.opcal.cloud.demo.sd.account.entity.Account;

public interface AccountService extends AccountApiService {

	Account findUserAccount(String userId);

}
